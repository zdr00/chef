#ifndef HORSE_h
#define HORSE_H

int substract_values(const void * a, const void * b);

int minimal_dif(int size, int * values);

#endif