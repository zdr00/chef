// gcc -c vector.c -o vector.o
// ar rcs libvector.a vector.o
// gcc horse.c libvector.a -o horse && ./horse
// 4 9 1 32 13

#include <stdio.h>
#include "horse.h"
#include "vector.h"

int substract_values(const void * a, const void * b){	
	int c = *(int*)a - *(int*)b;
	return c;
}

void print_int(void * elem, void * aux){
	printf("%d ", *((int * ) elem));
}

/*int minimal_dif(int size, int * values){
	int aux = 9999999;
	int i, n;
	for (i = 0; i < size; i++)
	{
		n = substract_values(values[i+1], values[i]);
		if(n<aux){
			aux = n;
		}
	}
	return aux;
}
*/
int main(){

	int testno;
	scanf("%d", &testno);
	int m;
	for(m = 0; m<testno; m++){		
		Vector v;	
		int a,b;
		scanf("%d",&a);	
		VectorNew(&v, sizeof(int), a);	
		int c = a;
		while(a-- > 0){	
			scanf("%d",&b);	
			VectorAppend(&v, &b);
		}

		//VectorMap(&v, print_int, NULL);
		VectorSort(&v, substract_values);
		//VectorMap(&v, print_int, NULL);


		int aux = 9999999;
		int h, n;	
		int v1, v2;	

		for (h = 0; h < c-1; h++)
		{
			VectorNth(&v, &v1, h+1);
			VectorNth(&v, &v2, h);

			//n = substract_values(v1,v2);
			if((v1-v2)<aux){
				aux = v1-v2;
			}
		}

		printf("%d\n", aux);
	}

	//printf("%d\n", aux);
	//int m[] = {1, 4, 9, 13, 32};	
	//printf("%d\n",minimal_dif(5, m));
	
	return 0;
}