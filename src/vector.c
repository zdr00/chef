#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "vector.h"

void VectorNew(Vector *v, int elemSize, int initialAllocation){
  v -> elemSize = elemSize;
  v -> initialAllocation = initialAllocation;
  v -> nelems = 0;
  v -> elems = malloc(v ->elemSize * v->initialAllocation);
  return;
}

void VectorDispose(Vector *v){
  free(v -> elems);
}

void VectorAppend(Vector *v, const void *elemAddr){
  if(v->nelems == v->initialAllocation){
    v->initialAllocation *= 2;
    v->elems = realloc(v->elems, v->initialAllocation * v->elemSize);
  }
  
  memcpy((char *)v->elems + v->nelems * v->elemSize,elemAddr, v->elemSize );
  v->nelems ++;
  return;
}

 int VectorLength (const Vector *v){
   return v->nelems;
 }


 void VectorReplace(Vector *v, const void *elemAddr, int position){
   memcpy((char *) v->elems + position*v->elemSize, elemAddr, v->elemSize);
   return;
 }

 void VectorInsert(Vector *v, const void *elemAddr, int position){
   memcpy((char *)v->elems + (position * v->elemSize) + v->elemSize,(char *)v->elems + position * v->elemSize ,(v->nelems - position)* v->elemSize);
   memcpy((char *) v->elems + position*v->elemSize, elemAddr, v->elemSize);
   return;
 }

 void VectorNth(const Vector *v, void *elemaddr, int position){
   memcpy(elemaddr,(char *)v->elems + position * v->elemSize, v->elemSize );
   return ;
 }


 void VectorMap(Vector *v, VectorMapFunction mapfn, void *auxData){
   void *elemAddr;
   int i;
   for (i = 0; i < v-> nelems; i++) {
     elemAddr = (char *)v->elems + i*v->elemSize;
     mapfn(elemAddr, auxData);
   }
   return;
 }

 void VectorSort(Vector *v, VectorCompareFunction comparefn){
   qsort(v->elems, v->nelems, v->elemSize, comparefn);
   return;
 }
